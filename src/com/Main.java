package com;

import java.util.Scanner;

public class Main
{
    private static final int arraySize = 6;
    
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int[] inputArray = new int[arraySize];
        for (int i = 0; i < arraySize; i++)
        {
            String in = sc.nextLine();
            try
            {
                inputArray[i] = Integer.valueOf(in);
            }
            catch (NumberFormatException ex)
            {
                System.out.println("Invalid input. Using default of 1 instead.");
                inputArray[i] = 1;
            }
        }
        SortingAlgorithm.abc(inputArray);
        for (int i = 0; i < inputArray.length; i++)
        {
            System.out.print(inputArray[i] + ", ");
        }
    }
}
