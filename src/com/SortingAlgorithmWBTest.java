package com;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Provides white box test methods for the method SortingAlgorithm.abc(int[]).
 */
public class SortingAlgorithmWBTest
{
    @Test
    public void fullStatementCoverage()
    {
        int[] arr = new int[]{2,1};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{1,2}, arr);
    }
    
    @Test
    public void fullEdgeCoverage()
    {
        int[] arr = new int[]{3,2,1};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{1,2,3}, arr);
    }

    @Test
    public void pathCoverage()
    {
        int[] arr1 = new int[]{1};
        SortingAlgorithm.abc(arr1);
        Assert.assertArrayEquals(new int[]{1},arr1);

        int[] arr2 = new int[]{1,2};
        SortingAlgorithm.abc(arr2);
        Assert.assertArrayEquals(new int[]{1,2},arr2);

        int[] arr3 = new int[]{2,1};
        SortingAlgorithm.abc(arr3);
        Assert.assertArrayEquals(new int[]{1,2}, arr3);

        int[] arr = new int[]{2, 3, 1};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{1, 2, 3}, arr);
    }
    
    @Test
    public void fullConditionCoverage()
    {
        int[] arr = new int[]{3,7,6,4,5,2};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{2,3,4,5,6,7}, arr);
    }
}