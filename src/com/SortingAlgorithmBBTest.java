package com;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class SortingAlgorithmBBTest
{
    @Test
    public void onlyPositiveValues()
    {
        int[] arr = new int[]{10, 3, 6, 1, 7, 5, 8, 4};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{1, 3, 4, 5, 6, 7, 8, 10}, arr);
    }
    
    @Test
    public void onlyPositiveValuesInclZero()
    {
        int[] arr = new int[]{10, 3, 6, 1, 7, 0, 5, 8, 4};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{0, 1, 3, 4, 5, 6, 7, 8, 10}, arr);
    }
    
    @Test
    public void onlyNegativeValues()
    {
        int[] arr = new int[]{-10, -3, -6, -1, -7, -5, -8, -4};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{-10, -8, -7, -6, -5, -4, -3, -1}, arr);
    }
    
    @Test
    public void onlyNegativeValuesInclZero()
    {
        int[] arr = new int[]{-10, -3, -6, -1, -7, 0, -5, -8, -4};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{-10, -8, -7, -6, -5, -4, -3, -1, 0}, arr);
    }
    
    @Test
    public void mixedNumbers()
    {
        int[] arr = new int[]{ 3, -4, 6, -2, 9, 10};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{-4, -2, 3, 6, 9, 10}, arr);
    }
    
    @Test
    public void mixedNumbersInclZero()
    {
        int[] arr = new int[]{ 3, -4, 6, -2, 9, 0, 10};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{-4, -2, 0, 3, 6, 9, 10}, arr);
    }
    
    @Test
    public void inOrder()
    {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, arr);
    }
    
    @Test
    public void invertedOrder()
    {
        int[] arr = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, arr);
    }
    
    @Test
    public void valuePairAtBeginning()
    {
        int[] arr = new int[]{5, 5, 2, 6, 1, 3, 4, 0};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{0, 1, 2, 3, 4, 5, 5, 6}, arr);
    }
    
    @Test
    public void valuePairInMiddle()
    {
        int[] arr = new int[]{5, 2, 6, 1, 3, 3, 4, 0};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{0, 1, 2, 3, 3, 4, 5, 6}, arr);
    }
    
    @Test
    public void valuePairAtEnd()
    {
        int[] arr = new int[]{5, 2, 6, 1, 3, 0, 4, 4};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{0, 1, 2, 3, 4, 4, 5, 6}, arr);
    }
    
    
    @Test
    public void maxValuePairAtStart()
    {
        int[] arr = new int[]{7, 7, 3, 4};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{3, 4, 7, 7}, arr);
    }
    
    @Test
    public void maxValuePairAtEnd()
    {
        int[] arr = new int[]{ 2, 3, 4, 4};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{2, 3, 4, 4}, arr);
    }
    
    @Test
    public void minValuePairAtStart()
    {
        int[] arr = new int[]{2, 2, 5, 3};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{2, 2, 3, 5}, arr);
    }
    
    @Test
    public void minValuePairAtEnd()
    {
        int[] arr = new int[]{7, 4, 5, 3, 2, 2};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{2, 2, 3, 4, 5, 7}, arr);
    }
    
    @Test
    public void allEqualArray()
    {
        int[] arr = new int[]{6,6,6,6};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{6,6,6,6}, arr);
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void emptyArray()
    {
        int[] arr = new int[]{};
        SortingAlgorithm.abc(arr);
    }
    
    @Test(expected = NullPointerException.class)
    public void nullArray()
    {
        int[] arr = null;
        SortingAlgorithm.abc(arr);
    }
    
    @Test
    public void minimalArraySize()
    {
        int[] arr = new int[]{7};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{7}, arr);
    }
    
    @Test
    public void maxInt()
    {
        int[] arr = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 1};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 1, Integer.MAX_VALUE}, arr);
    }
    
    @Test
    public void minInt()
    {
        int[] arr = new int[]{Integer.MIN_VALUE + 4, Integer.MIN_VALUE + 5, Integer.MIN_VALUE};
        SortingAlgorithm.abc(arr);
        Assert.assertArrayEquals(new int[]{Integer.MIN_VALUE, Integer.MIN_VALUE + 4, Integer.MIN_VALUE + 5}, arr);
    }
    
    @Test
    public void randomTest()
    {
        int testSize = 6;
        int[] arr = new int[testSize];
        int[] arrSort = new int[testSize];
        for (int i = 0; i < testSize; i++)
        {
            int z = (int)(Math.random() * 10);
            arr[i] = z;
            arrSort[i] = z;
        }
        SortingAlgorithm.abc(arr);
        Arrays.sort(arrSort);
        Assert.assertArrayEquals(arrSort,arr);
    }
}
